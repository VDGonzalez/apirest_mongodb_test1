var mongoose = require('mongoose');  

var Schema = mongoose.Schema;

var tvshowSchema = new Schema({
  title:    { type: String },
  year:     { type: Number },
  country:  { type: String },
  poster:   { type: String },
  seasons:  { type: Number },
  genre:    { type: String, enum: ['Drama', 'Fantasy', 'Sci-Fi', 'Thriller', 'Comedy'] },
  summary:  { type: String }
});

// {
//   "title": "LOST",
//   "year": 2004,
//   "country": "USA",
//   "poster": "http://www.poster.com/1.jpg",
//   "seasons": 6,
//   "genre": "Sci-Fi",
//   "summary": "..."
// }

var TVShow  = mongoose.model('TVShow', tvshowSchema);
// var TVShow  = mongoose.model('TVShow');

//GET - Return all tvshows in the DB
exports.findAllTVShows = function(req, res) {  
    TVShow.find(function(err, tvshows) {
    if(err) res.send(500, err.message);

    console.log('GET /tvshows')
        res.status(200).jsonp(tvshows);
    });
};

//GET - Return a TVShow with specified ID
exports.findById = function(req, res) {  
    TVShow.findById(req.params.id, function(err, tvshow) {
    if(err) return res.send(500, err.message);

    console.log('GET /tvshow/' + req.params.id);
        res.status(200).jsonp(tvshow);
    });
};

//POST - Insert a new TVShow in the DB
exports.addTVShow = function(req, res) {  
    console.log('POST');
    console.log(req.body);

    var tvshow = new TVShow({
        title:    req.body.title,
        year:     req.body.year,
        country:  req.body.country,
        poster:   req.body.poster,
        seasons:  req.body.seasons,
        genre:    req.body.genre,
        summary:  req.body.summary
    });

    tvshow.save(function(err, tvshow) {
        if(err) return res.status(500).send( err.message);
    res.status(200).jsonp(tvshow);
    });
};

//PUT - Update a register already exists
exports.updateTVShow = function(req, res) {  
    TVShow.findById(req.params.id, function(err, tvshow) {
        tvshow.title   = req.body.title;
        tvshow.year    = req.body.year;
        tvshow.country = req.body.country;
        tvshow.poster  = req.body.poster;
        tvshow.seasons = req.body.seasons;
        tvshow.genre   = req.body.genre;
        tvshow.summary = req.body.summary;

        tvshow.save(function(err) {
            if(err) return res.status(500).send(err.message);
      res.status(200).jsonp(tvshow);
        });
    });
};

//DELETE - Delete a TVShow with specified ID
exports.deleteTVShow = function(req, res) {  
    TVShow.findById(req.params.id, function(err, tvshow) {
        tvshow.remove(function(err) {
            if(err) return res.status(500).send(err.message);
      res.status(200).send();
        })
    });
};

