var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');  
mongoose.connect('mongodb://localhost/tvshows');

var TVShowCtrl = require('./../controllers/tvshows');

// API routes
var tvshows = express.Router();

router.route('/tvshows')  
  .get(TVShowCtrl.findAllTVShows)
  .post(TVShowCtrl.addTVShow);

router.route('/tvshows/:id')  
  .get(TVShowCtrl.findById)
  .put(TVShowCtrl.updateTVShow)
  .delete(TVShowCtrl.deleteTVShow);  

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
